msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-24 00:39+0000\n"
"PO-Revision-Date: 2022-12-20 22:48-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: notifications/notificationfilemenu.cpp:109
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: notifications/notificationfilemenu.cpp:123
#, kde-format
msgid "&Copy"
msgstr ""

#: notifications/notificationfilemenu.cpp:131
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr ""

#: notifications/notificationfilemenu.cpp:176
#, kde-format
msgid "Properties"
msgstr ""

#: qml/actiondrawer/quicksettings/QuickSettingsFullDelegate.qml:98
#, kde-format
msgid "On"
msgstr ""

#: qml/actiondrawer/quicksettings/QuickSettingsFullDelegate.qml:98
#, kde-format
msgid "Off"
msgstr ""

#: qml/dataproviders/SignalStrengthInfo.qml:17
#, kde-format
msgid "SIM Locked"
msgstr ""

#: qml/statusbar/indicators/BatteryIndicator.qml:36
#, kde-format
msgid "%1%"
msgstr ""

#: qml/volumeosd/AudioApplet.qml:59
#, kde-format
msgid "Outputs"
msgstr ""

#: qml/volumeosd/AudioApplet.qml:89
#, kde-format
msgid "Inputs"
msgstr ""

#: qml/volumeosd/AudioApplet.qml:120
#, kde-format
msgid "Playback Streams"
msgstr ""

#: qml/volumeosd/AudioApplet.qml:173
#, kde-format
msgid "Recording Streams"
msgstr ""

#: qml/volumeosd/DeviceListItem.qml:25
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr ""

#: qml/volumeosd/DeviceListItem.qml:34
#, kde-format
msgid "Device name not found"
msgstr ""

#: qml/volumeosd/ListItemBase.qml:96
#, kde-format
msgid "Show additional options for %1"
msgstr ""

#: qml/volumeosd/ListItemBase.qml:152
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr ""

#: qml/volumeosd/ListItemBase.qml:230
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr ""

#: qml/volumeosd/ListItemBase.qml:245
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr ""

#: qml/volumeosd/StreamListItem.qml:21
#, kde-format
msgid "Notification Sounds"
msgstr ""

#: qml/volumeosd/StreamListItem.qml:24
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr ""

#: qml/volumeosd/StreamListItem.qml:29
#, kde-format
msgid "Stream name not found"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:104
#, kde-format
msgid "Unmute"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:104
#, kde-format
msgid "Mute"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:128
#, kde-format
msgid "100%"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:138
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:158
#, kde-format
msgid "Open audio settings"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:171
#, kde-format
msgid "Audio Settings"
msgstr ""

#: qml/volumeosd/VolumeOSD.qml:178
#, kde-format
msgid "Toggle showing audio streams"
msgstr ""

#: qml/widgets/krunner/KRunnerScreen.qml:84
#: qml/widgets/krunner/KRunnerWidget.qml:181
#, kde-format
msgid "Search…"
msgstr ""

#: qml/widgets/mediacontrols/MediaControlsWidget.qml:142
#, kde-format
msgid "No media playing"
msgstr ""

#: qml/widgets/mediacontrols/MediaControlsWidget.qml:175
#, kde-format
msgid "Previous track"
msgstr ""

#: qml/widgets/mediacontrols/MediaControlsWidget.qml:189
#, kde-format
msgid "Play or Pause media"
msgstr ""

#: qml/widgets/mediacontrols/MediaControlsWidget.qml:205
#, kde-format
msgid "Next track"
msgstr ""

#: qml/widgets/notifications/NotificationFooterActions.qml:55
#, kde-format
msgctxt "Reply to message"
msgid "Reply"
msgstr ""

#: qml/widgets/notifications/NotificationReplyField.qml:36
#, kde-format
msgctxt "Text field placeholder"
msgid "Type a reply…"
msgstr ""

#: qml/widgets/notifications/NotificationReplyField.qml:55
#, kde-format
msgctxt "@action:button"
msgid "Send"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:14
#, kde-format
msgctxt "Job name, e.g. Copying is paused"
msgid "%1 (Paused)"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:19
#, kde-format
msgctxt "Job name, e.g. Copying has failed"
msgid "%1 (Failed)"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:21
#, kde-format
msgid "Job Failed"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:24
#, kde-format
msgctxt "Job name, e.g. Copying has finished"
msgid "%1 (Finished)"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:26
#, kde-format
msgid "Job Finished"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:49
#, kde-format
msgctxt "Notification was added minutes ago, keep short"
msgid "%1m ago"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:81
#, kde-format
msgctxt "seconds remaining, keep short"
msgid "%1 s remaining"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:84
#, kde-format
msgctxt "minutes remaining, keep short"
msgid "%1m remaining"
msgstr ""

#: qml/widgets/notifications/NotificationsUtils.js:87
#, kde-format
msgctxt "hours remaining, keep short"
msgid "%1h remaining"
msgstr ""

#: qml/widgets/notifications/NotificationsWidget.qml:177
#, kde-format
msgid "Notification service not available"
msgstr ""

#: qml/widgets/notifications/NotificationsWidget.qml:186
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2'"
msgstr ""

#: qml/widgets/notifications/NotificationsWidget.qml:322
#, kde-format
msgid "Show Fewer"
msgstr ""

#: qml/widgets/notifications/NotificationsWidget.qml:324
#, kde-format
msgctxt "Expand to show n more notifications"
msgid "Show %1 More"
msgstr ""

#: qml/widgets/notifications/NotificationsWidget.qml:383
#, kde-format
msgid "Clear All Notifications"
msgstr ""

#: qml/widgets/notifications/ThumbnailStrip.qml:134
#, kde-format
msgid "More Options…"
msgstr ""
