msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-12 00:39+0000\n"
"PO-Revision-Date: 2018-11-20 23:16-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ui/ConnectDialog.qml:57
#, kde-format
msgid "Invalid input."
msgstr ""

#: ui/ConnectionItemDelegate.qml:125
#, kde-format
msgid "Connect to"
msgstr ""

#: ui/main.qml:26
#, kde-format
msgid "Edit"
msgstr ""

#: ui/main.qml:87
#, kde-format
msgid "Wi-Fi"
msgstr ""

#: ui/main.qml:135
#, kde-format
msgid "Available Networks"
msgstr ""

#: ui/NetworkSettings.qml:15
#, kde-format
msgid "Add New Connection"
msgstr ""

#: ui/NetworkSettings.qml:36
#, kde-format
msgid "Save"
msgstr ""

#: ui/NetworkSettings.qml:73
#, kde-format
msgid "Hidden Network"
msgstr ""

#: ui/NetworkSettings.qml:96
#, kde-format
msgid "None"
msgstr ""

#: ui/NetworkSettings.qml:97
#, kde-format
msgid "WEP Key"
msgstr ""

#: ui/NetworkSettings.qml:98
#, kde-format
msgid "Dynamic WEP"
msgstr ""

#: ui/NetworkSettings.qml:99
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr ""

#: ui/NetworkSettings.qml:100
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr ""

#: ui/NetworkSettings.qml:146
#, kde-format
msgid "Authentication:"
msgstr ""

#: ui/NetworkSettings.qml:150
#, kde-format
msgid "TLS"
msgstr ""

#: ui/NetworkSettings.qml:150
#, kde-format
msgid "LEAP"
msgstr ""

#: ui/NetworkSettings.qml:150
#, kde-format
msgid "FAST"
msgstr ""

#: ui/NetworkSettings.qml:151
#, kde-format
msgid "Tunneled TLS"
msgstr ""

#: ui/NetworkSettings.qml:152
#, kde-format
msgid "Protected EAP"
msgstr ""

#: ui/NetworkSettings.qml:170
#, kde-format
msgid "Automatic"
msgstr ""

#: ui/NetworkSettings.qml:170
#, kde-format
msgid "Manual"
msgstr ""

#: ui/NetworkSettings.qml:186
#, kde-format
msgid "IP Address"
msgstr ""

#: ui/NetworkSettings.qml:204
#, kde-format
msgid "Gateway"
msgstr ""

#: ui/NetworkSettings.qml:222
#, kde-format
msgid "Network prefix length"
msgstr ""

#: ui/NetworkSettings.qml:241
#, kde-format
msgid "DNS"
msgstr ""

#: ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr ""
