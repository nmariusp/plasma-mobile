# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# SPDX-FileCopyrightText: 2024 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-07 00:38+0000\n"
"PO-Revision-Date: 2024-03-22 08:19+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.5\n"

#: dragstate.cpp:14
#, kde-format
msgid "Folder"
msgstr "תיקייה"

#: package/contents/ui/AppDrawerHeader.qml:27
#, kde-format
msgid "Applications"
msgstr "יישומונים"

#: package/contents/ui/FavouritesBar.qml:147
#: package/contents/ui/FavouritesBar.qml:220
#: package/contents/ui/FolderView.qml:270
#: package/contents/ui/HomeScreenPage.qml:214
#: package/contents/ui/HomeScreenPage.qml:293
#, kde-format
msgid "Remove"
msgstr "הסרה"

#: package/contents/ui/settings/AppletListViewer.qml:56
#, kde-format
msgid "Widgets"
msgstr "וידג׳טים"

#: package/contents/ui/settings/SettingsWindow.qml:78
#, kde-format
msgid "Homescreen Settings"
msgstr "הגדרות מסך הבית"

#: package/contents/ui/settings/SettingsWindow.qml:87
#, kde-format
msgid "Icons"
msgstr "סמלים"

#: package/contents/ui/settings/SettingsWindow.qml:118
#, kde-format
msgid "Number of rows"
msgstr "מספר השורות"

#: package/contents/ui/settings/SettingsWindow.qml:119
#, kde-format
msgid "Number of columns"
msgstr "מספר העמודות"

#: package/contents/ui/settings/SettingsWindow.qml:123
#, kde-format
msgid "Size of icons on homescreen"
msgstr "גודל הסמלים במסך הבית"

#: package/contents/ui/settings/SettingsWindow.qml:162
#, kde-format
msgid "The rows and columns will swap depending on the screen rotation."
msgstr "השורות והעמודות תתחלפנה בהתאם לכיוון המסך."

#: package/contents/ui/settings/SettingsWindow.qml:166
#, kde-format
msgid "Homescreen"
msgstr "מסך הבית"

#: package/contents/ui/settings/SettingsWindow.qml:172
#, kde-format
msgid "Show labels on homescreen"
msgstr "הצגת תוויות במסך הבית"

#: package/contents/ui/settings/SettingsWindow.qml:185
#, kde-format
msgid "Show labels in favorites bar"
msgstr "הצגת תוויות בסרגל המועדפים"

#: package/contents/ui/settings/SettingsWindow.qml:198
#, kde-format
msgid "Page transition effect"
msgstr "אפקט החלפת עמוד"

#: package/contents/ui/settings/SettingsWindow.qml:204
#, kde-format
msgid "Slide"
msgstr "גלישה"

#: package/contents/ui/settings/SettingsWindow.qml:205
#, kde-format
msgid "Cube"
msgstr "קובייה"

#: package/contents/ui/settings/SettingsWindow.qml:206
#, kde-format
msgid "Fade"
msgstr "עמעום"

#: package/contents/ui/settings/SettingsWindow.qml:207
#, kde-format
msgid "Stack"
msgstr "ערימה"

#: package/contents/ui/settings/SettingsWindow.qml:208
#, kde-format
msgid "Rotation"
msgstr "סיבוב"

#: package/contents/ui/settings/SettingsWindow.qml:223
#, kde-format
msgid "Favorites Bar"
msgstr "סרגל מועדפים"

#: package/contents/ui/settings/SettingsWindow.qml:240
#, kde-format
msgctxt "@title:group settings group"
msgid "Wallpaper"
msgstr "תמונת רקע"

#: package/contents/ui/settings/SettingsWindow.qml:246
#, kde-format
msgctxt "@option:check"
msgid "Show wallpaper blur effect"
msgstr "להציג אפקט טשטש תמונת רקע"

#: package/contents/ui/settings/SettingsWindow.qml:257
#, kde-format
msgid "General"
msgstr "כללי"

#: package/contents/ui/settings/SettingsWindow.qml:264
#, kde-format
msgctxt "@action:button"
msgid "Switch between homescreens and more wallpaper options"
msgstr "להחליף בין מסכי הבית ועוד אפשרויות תמונות רקע"

#: package/contents/ui/settings/SettingsWindow.qml:291
#, kde-format
msgid "Export layout to"
msgstr "ייצוא הפריסה אל"

#: package/contents/ui/settings/SettingsWindow.qml:310
#, kde-format
msgid "Import layout from"
msgstr "ייבוא פריסה מתוך"

#: package/contents/ui/settings/SettingsWindow.qml:321
#: package/contents/ui/settings/SettingsWindow.qml:328
#, kde-format
msgid "Export Status"
msgstr "מצב ייצוא"

#: package/contents/ui/settings/SettingsWindow.qml:322
#, kde-format
msgid "Failed to export to %1"
msgstr "הייצוא אל %1 נכשל"

#: package/contents/ui/settings/SettingsWindow.qml:329
#, kde-format
msgid "Homescreen layout exported successfully to %1"
msgstr "פריסת מסך הבית יוצאה בהצלחה אל %1"

#: package/contents/ui/settings/SettingsWindow.qml:335
#, kde-format
msgid "Confirm Import"
msgstr "אישור ייבוא"

#: package/contents/ui/settings/SettingsWindow.qml:336
#, kde-format
msgid "This will overwrite your existing homescreen layout!"
msgstr "פעולה זו תדרוס את פריסת מסך הבית הנוכחית שלך!"
